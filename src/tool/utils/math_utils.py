"""
Example math utils file
"""


def multiply(first: int, second: int) -> int:
    """
    Multiplication function
    :param first: first integer to multiply
    :param second: second integer to multiply
    :return: the product of the two integers
    """
    return first * second
