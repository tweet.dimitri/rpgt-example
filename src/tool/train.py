"""
Train file
"""
import joblib
import pandas as pd
from sklearn import svm
from sklearn.model_selection import train_test_split


def train() -> None:
    """
    Train function
    """
    data = pd.read_csv("data/preprocessed.csv")
    features = data.filter(
        items=["sepal_length", "sepal_width", "petal_length", "petal_width"]
    )
    labels = data["class"]
    x_train, x_validate, y_train, y_validate = train_test_split(
        features, labels, test_size=0.1, random_state=1, shuffle=True
    )
    model = svm.SVC()
    model.fit(x_train, y_train)

    joblib.dump(model, "results/models/model.joblib")
    pd.concat([x_validate, y_validate], axis=1).to_csv(
        "data/validation.csv", index=False
    )


if __name__ == "__main__":
    train()
