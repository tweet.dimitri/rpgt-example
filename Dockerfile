FROM python:3.8.10-slim

RUN apt-get update \
	&& apt-get install -y --no-install-recommends git \
	&& pip install pipenv

COPY . /root/project/

WORKDIR /root/project

ENV PIPENV_VENV_IN_PROJECT="enabled"

RUN pipenv install

CMD bash
