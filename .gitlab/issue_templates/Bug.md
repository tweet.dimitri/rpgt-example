### Summary
<!--- (Summarize the bug encountered concisely) -->

### Steps to reproduce
<!--- (How one can reproduce the issue - this is very important) -->

### What is the current *bug* behavior?
<!--- (What actually happens) -->

### What is the expected *correct* behavior?
<!--- (What you should see instead) -->

### Relevant logs and/or screenshots

### Possible fixes
<!--- (If you can, link to the line of code that might be responsible for the problem) -->

## Priority
<!-- PRIORITY GUIDELINES
LOWEST = trivial problem with little to no impact on progress (e.g. refactor to consistent naming convention)
LOW = minor problem with easy workaround (e.g. updating wiki)
MEDIUM = can potentially block progress, other issues might depend on it (e.g. add zooming functionality)
HIGH = serious problem that will block progress if not done early (e.g. creating an api point)
HIGHEST = problem blocks progress, can't continue without it (e.g. serious bug fix)
-->
<!-- Set the priority -->
<!-- [Highest, Critical, Alarming, Low, Lowest] -->
/label ~Alarming

<!-- Explain why you choose this priority -->


<!---
LINES BELOW WILL SET GITLAB ISSUE PROPERTIES
--->
/label ~Bug
<!-- Assign the team members working on the feature -->
/assign @team-member1 @team-member2
<!-- Estimate the amount of hours needed to complete the feature -->
/estimate 1h
<!-- Set the correct milestone -->
/milestone %"Sprint x"
