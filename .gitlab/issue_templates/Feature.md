## Context / Problem to solve

### Who
<!-- Set the chosen option by making it bold -->
The user | The developer | The stakeholder
<!-- Explain why you choose this -->


### What
<!-- State what the issue is. -->


### Why
<!-- State why the issue is needed. -->


## Feature
<!-- Provide possible suggestions on solutions. If multiple solutions are possible consider listing them all -->


<!-- list definition of done -->
Definition of done:
- [ ] Feature 1
- [ ] All new code is tested with unit tests with at least 90% of branch coverage
- [ ] All new code is documented

## Action plan

<!-- Suggest next steps -->
Action plan:
- [ ] Step 1

## Priority
<!-- PRIORITY GUIDELINES
LOWEST = trivial problem with little to no impact on progress (e.g. refactor to consistent naming convention)
LOW = minor problem with easy workaround (e.g. updating wiki)
MEDIUM = can potentially block progress, other issues might depend on it (e.g. add zooming functionality)
HIGH = serious problem that will block progress if not done early (e.g. creating an api point)
HIGHEST = problem blocks progress, can't continue without it (e.g. serious bug fix)
-->
<!-- Set the priority -->
<!-- [Highest, Critical, Alarming, Low, Lowest] -->
/label ~Alarming

<!-- Explain why you choose this priority -->


<!---
LINES BELOW WILL SET GITLAB ISSUE PROPERTIES
--->
/label ~Bug
<!-- Assign the team members working on the feature -->
/assign @team-member1 @team-member2
<!-- Estimate the amount of hours needed to complete the feature -->
/estimate 1h
<!-- Set the correct milestone -->
/milestone %"Sprint x"
