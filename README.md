# tool

## Getting Started
To get started with your generated repository, please first check the next steps available on [the wiki](https://gitlab.com/tweet.dimitri/rpgt/-/wikis/Next%20steps) of RPGT.

## Requirements
In order to contribute to this package, you will need to have the following components installed on your system:
- pipenv
- dvc
- scipy
- numpy
- pandas
- sklearn
- mllint
- isort
- pylint
- mypy
- black
- bandit
- pytest
- pytest-html
- setuptools

You can easily install them using `pipenv install`.

## Reproducibility
Because this package uses [DVC](https://dvc.org/), reproducing results becomes an easy task.
To run all the required scripts in the right order, you only need to run `pipenv run dvc repro` (provided that you have already run `pipenv install`).
This will execute the pipeline defined in `dvc.yaml` and run the correct scripts in the right order.
Afterwards, you'll be able to see the results in the `results` directory.

## License
    
This project is MIT licensed. Please see the [LICENSE](LICENSE.md) file for more information.

## Authors

- [Author 1]
- [Author 2]

Please see the [CONTRIBUTING](CONTRIBUTING.md) file for more information on how to contribute to this project.
